/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CalculadoraSCM;

import static java.lang.Math.pow;
import javax.swing.JOptionPane;

/**
 *
 * @author Erick
 */
public class Potencia {
    
    public boolean verificarPotencia(String operacion){
        boolean op = false;
        if(operacion.contains("^"))
            op=true;
        return op;
    }
    public double[] obtenerTerminos(String operacion){
        double[] terminos= new double[2];
        
        int n=operacion.length();
        for(int i=0;i<n;i++){
            if(operacion.charAt(i)=='^'){
                terminos[0]=Double.parseDouble(operacion.substring(0,i));
                terminos[1]=Double.parseDouble(operacion.substring(i+1,n));
            }
                
        }
        return terminos;
    }
    
    public double potencia(String entrada){
        double[] terminos = new double[2];
        terminos=this.obtenerTerminos(entrada);
        double result=0;
        if(terminos[0]!= 0 && terminos[1] !=0){
            result=Math.pow(terminos[0],terminos[1]);
        } else{
            JOptionPane.showMessageDialog(null, "Indeterminación");
        }
        return result;
    }
}

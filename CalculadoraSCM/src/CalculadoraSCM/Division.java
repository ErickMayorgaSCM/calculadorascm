
package CalculadoraSCM;

import javax.swing.JOptionPane;

public class Division {
    
    CalculadoraGUI cd;
    public Division(){
        
    }
    
    public boolean validaroperacionDivision(String operacion){
        boolean divisionONo=false;
        if(operacion.contains("/")){
            return true;
        }
        return false;
    }
    public double[] obtenerTerminosporDivision(String operacion){
        double[] terminos= new double[2];
        String[] cadenas = new String[2];
        cadenas = operacion.split("/");
        terminos[0]=Double.parseDouble(cadenas[0]);
        terminos[1]=Double.parseDouble(cadenas[1]);
        return terminos;
    }
    public double dividir(String entradaCalculadora){
        double[] terminos = new double[2];
        terminos=this.obtenerTerminosporDivision(entradaCalculadora);
        double cociente=0;
        if(terminos[1]!= 0){
            cociente=terminos[0]/terminos[1];
        } else{
            JOptionPane.showMessageDialog(null, "No existe division para cero.");
        }
        return cociente;
    }
}

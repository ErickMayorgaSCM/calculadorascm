/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CalculadoraSCM;

import javax.swing.JOptionPane;

/**
 *
 * @author Rommel 
 */
public class Multiplicacion {
    public boolean verificarMultiplicacion(String operacion){
        boolean op = false;
        if(operacion.contains("*"))
            op=true;
        return op;
    }
    public double[] obtenerTerminos(String operacion){
        double[] terminos= new double[2];
        String[] cadenas = new String[2];
        cadenas = operacion.split("\\*");
        terminos[0]=Double.parseDouble(cadenas[0]);
        terminos[1]=Double.parseDouble(cadenas[1]);
        return terminos;
    }
    
    public double multiplicar(String cadena){
        double resultado; 
        double [] numeros= new double[2];
        numeros = this.obtenerTerminos(cadena);
        resultado= numeros[0]*numeros[1];
        return resultado; 
    }
}
